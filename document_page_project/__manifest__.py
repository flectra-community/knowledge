# Copyright 2019 ForgeFlow S.L. (https://www.forgeflow.com) - Lois Rilo
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Document Page Project",
    "summary": "This module links document pages to projects",
    "version": "2.0.1.0.1",
    "development_status": "Production/Stable",
    "category": "Project",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/knowledge",
    "license": "AGPL-3",
    "depends": ["project", "document_page"],
    "data": ["views/document_page_views.xml", "views/project_project_views.xml"],
    "installable": True,
}
