# Flectra Community / knowledge

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[document_page_reference](document_page_reference/) | 2.0.1.0.0|         Include references on document pages
[document_url](document_url/) | 2.0.1.1.0| URL attachment
[knowledge](knowledge/) | 2.0.1.0.0| Knowledge
[document_page_approval](document_page_approval/) | 2.0.1.0.0| Document Page Approval
[document_page_project](document_page_project/) | 2.0.1.0.1| This module links document pages to projects
[document_page_group](document_page_group/) | 2.0.1.0.0|         Define access groups on documents
[document_page](document_page/) | 2.0.1.0.1| Document Page


